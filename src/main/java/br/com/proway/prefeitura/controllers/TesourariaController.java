package br.com.proway.prefeitura.controllers;

import br.com.proway.prefeitura.model.Tesouraria;
import br.com.proway.prefeitura.repository.TesourariaRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class TesourariaController {
    @Autowired
    private TesourariaRespository tesourariaRespository;

    @GetMapping("/tesouraria")
    public ArrayList<Tesouraria> getAllTesouraria() {
        return tesourariaRespository.findAll();
    }

    @GetMapping("/tesouraria/{id}")
    public Tesouraria getTesouraria(@PathVariable("id") long id) {
        return tesourariaRespository.findById(id);
    }

    //@RequestMapping(value = "/tesouraria/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PostMapping("/tesouraria/add")
    public ResponseEntity<Tesouraria> addTesouraria(@RequestBody Tesouraria tesouraria) {
        Tesouraria t;
        try{
            t = tesourariaRespository.save(tesouraria);
            return new ResponseEntity<>(t, HttpStatus.CREATED);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/tesouraria/{id}")
    public void deleteTesouraria(@PathVariable("id") long id){
        tesourariaRespository.deleteById(id);
    }

    @PutMapping("/tesouraria/{id}")
    public Tesouraria atualizarTesouraria(@PathVariable("id") long id, @RequestBody Tesouraria tesouraria){
        tesouraria.setId(id);
        return tesourariaRespository.save(tesouraria);
    }


}
