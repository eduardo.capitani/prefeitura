package br.com.proway.prefeitura.repository;

import br.com.proway.prefeitura.model.Tesouraria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface TesourariaRespository extends JpaRepository<Tesouraria, Long> {
    ArrayList<Tesouraria> findAll();
    Tesouraria findById(long id);
    Tesouraria save(Tesouraria pessoa);
    void deleteById(long id);
}
